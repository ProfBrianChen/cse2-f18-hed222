//Haydn Davies
//CSE 002
// take the origional cost of a check, decide the appropriate tip
// then the number of people who will be spilting the check, and print the final cost for one person

import java.util.Scanner; //imports scanner class from java
//
//
//
public class Check{
  //
  public static void main(String[] args){
    
    Scanner myScanner = new Scanner(System.in); //creates a scanner called my scanner that imports the scanner class from java. Which allows user to put in their own input
    System.out.print("Enter the original cost of the check in the form xx.xx: "); //prints a text with directions
    double checkCost = myScanner.nextDouble(); //Assigns the double that the user inputed as the cost of the check 
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): "); // more directions printed for user
    double tipPercent = myScanner.nextDouble(); // Assigns the tip % the user wants as a double to tipPercent
    tipPercent /= 100; //We want to convert the percentage into a decimal value
    System.out.print("Enter the number of people who went out to dinner: "); // directions for user 
    int numPeople = myScanner.nextInt(); //creates and assigns variable for number of people at dinner that the user declares
    double totalCost;
    double costPerPerson;
    int dollars, dimes, pennies;  //whole dollar amount of cost 
    totalCost = checkCost * (1 + tipPercent); // creates total cost with tip included
    costPerPerson = totalCost / numPeople; //get the whole amount, dropping decimal fraction
    dollars=(int)costPerPerson;//casts the value of dollars into a whole number int
    dimes=(int)(costPerPerson * 10) % 10; //casts the value of dimes into a whole number int
    pennies=(int)(costPerPerson * 100) % 10; //casts the value of pennies into a whole number int
    System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies); //prints final cost in terms of #dollars, dimes and pennies




    
  }
}

