//haydn davies lab05
//CSE 002
//patternA

import java.util.Scanner;

public class patternA{
    public static void main(String args[]){
        Scanner scan = new Scanner(System.in);
        System.out.println( "Give the number of loops you want to run (1-10): "); //asks the user to input a int value
        String garbage  ="";
        int input = 0;
        
        //makes sure the number of loops requested is an integer
        while(scan.hasNextInt()==false){
            garbage = scan.nextLine();
            System.out.println("Please give me an integer value");
        }
        input = scan. nextInt();
        
      //runs through the double for loop

        for(int row = 1; row <= input; row++){
            for(int col = 1; col <= row; col++){
                System.out.print(col + " ");
            }
            System.out.println("\n");
        }


	}
}