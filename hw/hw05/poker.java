//haydn davies hw05
//CSE 002

//This program does a lot, it makes sure the user is inputing an int value then it finds the probabilities of each kind of simple poker hand in many different variations of a random deck.
import java.util.Scanner;

public class poker{
    public static void main(String args[]){
        Scanner scan = new Scanner(System.in);
        System.out.println("How many hands do you want to run? (Please give me an int):"); //asks the user to input a int value
        String garbage  ="";
        int hands = 0;
        
        //makes sure the number of loops requested is an integer
        while(scan.hasNextInt()==false){
            garbage = scan.nextLine();
            System.out.println("Please give me an integer value");
        }
        hands = scan.nextInt();
        int count1 = 0;
        int count2 = 0;
        int count3 = 0;
        int count4 = 0;
        int count5 = 0;
        
        //runs a while loop when the desired number of loops is still greater than 0.
        while(hands>0){
        //     while(index< 5){
        //         int rand = (int)(Math.random()*52+1);
        //         intArray[index++]=rand;
        //     }
        
        //     hands--;
        // }

//calculates 5 random numbers
    int card1 = (int)(Math.random()*51+1);
    int card2 = (int)(Math.random()*51+1);
    int card3 = (int)(Math.random()*51+1);
    int card4 = (int)(Math.random()*51+1);
    int card5 = (int)(Math.random()*51+1);
    //makes sure that the five random numbers are not equal to each other. Because there are no 2 of spades for example in a card deck
    while(card1==card2||card1==card3||card1==card4||card1==card5||card2==card3||card2==card4||card2==card5||card3==card4||card3==card5||card4==card5){
        card1 = (int)(Math.random()*51+1);
        card2 = (int)(Math.random()*51+1);
        card3 = (int)(Math.random()*51+1);
        card4 = (int)(Math.random()*51+1);
        card5 = (int)(Math.random()*51+1);
    }
    int ace =0;
    int two =0;
    int three =0;
    int four=0;
    int five=0;
    int six=0;
    int seven=0;
    int eight=0;
    int nine =0;
    int ten=0;
    int jack=0;
    int queen=0;
    int king =0;
    
    int rand1 = card1 % 13;  //gets the remainder of card one out of thriteen
    //switch statement that handles random card 1, and finds what the random number face value is equivelent too, then adds one to that vaariable
        switch(rand1){
        case 1:
            ace++;
            break;
        case 2:
          two++;
          break;
        case 3:
          three++;
          break;
        case 4:
          four++;
          break;
        case 5:
          five++;
          break;
        case 6:
          six++;
          break;
        case 7:
          seven++;
          break;
        case 8:
          eight++;
          break;
        case 9:
            nine++;
            break;
        case 10:
            ten++;
          break;
        case 11:
          jack++;
          break;
        case 12:
          queen++;
          break;
        default:
          king++;
          break;
      
        }
    //switch statement that handles random card 2, and finds what the random number face value is equivelent too, then adds one to that vaariable
    int rand2 = card2 %13;
    
        switch(rand2){
        case 1:
            ace++;
            break;
        case 2:
          two++;
          break;
        case 3:
          three++;
          break;
        case 4:
          four++;
          break;
        case 5:
          five++;
          break;
        case 6:
          six++;
          break;
        case 7:
          seven++;
          break;
        case 8:
          eight++;
          break;
        case 9:
            nine++;
            break;
        case 10:
            ten++;
          break;
        case 11:
          jack++;
          break;
        case 12:
          queen++;
          break;
        default:
          king++;
          break;
      
        }
    //switch statement that handles random card 3, and finds what the random number face value is equivelent too, then adds one to that vaariable
    int rand3 = card3 %13;
    
        switch(rand3){
        case 1:
            ace++;
            break;
        case 2:
          two++;
          break;
        case 3:
          three++;
          break;
        case 4:
          four++;
          break;
        case 5:
          five++;
          break;
        case 6:
          six++;
          break;
        case 7:
          seven++;
          break;
        case 8:
          eight++;
          break;
        case 9:
            nine++;
            break;
        case 10:
            ten++;
          break;
        case 11:
          jack++;
          break;
        case 12:
          queen++;
          break;
        default:
          king++;
          break;
      
        }
    //switch statement that handles random card 4, and finds what the random number face value is equivelent too, then adds one to that vaariable
    int rand4 = card4 %13;
    
        switch(rand4){
        case 1:
            ace++;
            break;
        case 2:
          two++;
          break;
        case 3:
          three++;
          break;
        case 4:
          four++;
          break;
        case 5:
          five++;
          break;
        case 6:
          six++;
          break;
        case 7:
          seven++;
          break;
        case 8:
          eight++;
          break;
        case 9:
            nine++;
            break;
        case 10:
            ten++;
          break;
        case 11:
          jack++;
          break;
        case 12:
          queen++;
          break;
        default:
          king++;
          break;
      
        }
    //switch statement that handles random card 5, and finds what the random number face value is equivelent too, then adds one to that vaariable
    int rand5 = card5 %13;
    
        switch(rand5){
        case 1:
            ace++;
            break;
        case 2:
          two++;
          break;
        case 3:
          three++;
          break;
        case 4:
          four++;
          break;
        case 5:
          five++;
          break;
        case 6:
          six++;
          break;
        case 7:
          seven++;
          break;
        case 8:
          eight++;
          break;
        case 9:
            nine++;
            break;
        case 10:
            ten++;
          break;
        case 11:
          jack++;
          break;
        case 12:
          queen++;
          break;
        default:
          king++;
          break;
      
        }
    //adds a to count if four of a kind
    if(ace==4||two==4||three==4||four==4||five==4||six==4||seven==4||eight==4||nine==4||ten==4||jack==4||queen==4||king==4){
        count1++;
        // double prob = (4/52)*(3/51)*(2/50)*(1/49);
        // int num = (int)(prob*1000);
        // double probs = (num/1000.0);
        // System.out.println("you got a four of a kind, probability is: " + probs);
    }
    //adds a to count if three of a kind
    else if(ace==3||two==3||three==3||four==3||five==3||six==3||seven==3||eight==3||nine==3||ten==3||jack==3||queen==3||king==3){
        count2++;
        // double prob = (3/52)*(2/51)*(1/50);
        // int num = (int)(prob*1000);
        // double probs = (num/1000.0);
        // System.out.println("you got a three of a kind, probability is: " + probs);
    }
    //adds a to count if two pairs
    else if(ace==2&&two==2||ace==2&&three==2||ace==2&&four==2||ace==2&&five==2||ace==2&&six==2||ace==2&&seven==2||ace==2&&eight==2||ace==2&&nine==2||ace==2&&ten==2||ace==2&&jack==2||ace==2&&queen==2||ace==2&&king==2||
    two==2&&three==2||two==2&&four==2||two==2&&five==2||two==2&&six==2||two==2&&seven==2||two==2&&eight==2||two==2&&nine==2||two==2&&ten==2||two==2&&jack==2||two==2&&queen==2||two==2&&king==2||
    three==2&&four==2||three==2&&five==2||three==2&&six==2||three==2&&seven==2||three==2&&eight==2||three==2&&nine==2||three==2&&ten==2||three==2&&jack==2||three==2&&queen==2||three==2&&king==2||
    four==2&&five==2||four==2&&six==2||four==2&&seven==2||four==2&&eight==2||four==2&&nine==2||four==2&&ten==2||four==2&&jack==2||four==2&&queen==2||four==2&&king==2||
    five==2&&six==2||five==2&&seven==2||five==2&&eight==2||five==2&&nine==2||five==2&&ten==2||five==2&&jack==2||five==2&&queen==2||five==2&&king==2||
    six==2&&seven==2||six==2&&eight==2||six==2&&nine==2||six==2&&ten==2||six==2&&jack==2||six==2&&queen==2||six==2&&king==2||
    seven==2&&eight==2||seven==2&&nine==2||seven==2&&ten==2||seven==2&&jack==2||seven==2&&queen==2||seven==2&&king==2||
    eight==2&&nine==2||eight==2&&ten==2||eight==2&&jack==2||eight==2&&queen==2||eight==2&&king==2||
    nine==2&&ten==2||nine==2&&jack==2||nine==2&&queen==2||nine==2&&king==2||
    ten==2&&jack==2||ten==2&&queen==2||ten==2&&king==2||
    jack==2&&queen==2||jack==2&&king==2||
    queen==2&&king==2){
        count3++;
        // double prob = (2/52)*(1/51)*(2/50)*(1/49)+(2/52)*(1/51)*(2/50)*(1/49);
        // int num = (int)(prob*1000);
        // double probs = (num/1000.0);
        // System.out.println("you got a four of a kind, probability is: " + probs);
    }
    //adds a to count if one pair
    else if(ace==2||two==2||three==2||four==2||five==2||six==2||seven==2||eight==2||nine==2||ten==2||jack==2||queen==2||king==2){
        count4++;
        // double prob = (2/52)*(1/51);
        // int num = (int)(prob*1000);
        // double probs = (num/1000.0);
        // System.out.println("you got one pair, probability is: " + probs);
    } 
    //adds a to count if none are similar
    else{
        count5++;
        //System.out.println("You got five different cards");
    }
    
    hands--; //subtracts one loop from the total hands, then continues to run the while loop
 }
 //gets the totaL NUMBER of loops
    int total = count1+count2+count3+count4+count5;
    //gts the probability for four of a kind
    double prob1 = (double)(count1)/(total);
    int num1 = (int)(prob1*1000);
    double probs1 = (num1/1000.00);
    
    //calculates probability for three of a kind
    double prob2 = (double)(count2)/(total);
    int num2 = (int)(prob2*1000);
    double probs2 = (num2/1000.00);
    
    //calculates probability for two pairs
    double prob3 = (double)(count3)/(total);
    int num3 = (int)(prob3*1000);
    double probs3 = (num3/1000.00);
    
    //calculates probability for one pair
    double prob4 = (double)(count4)/(total);
    int num4 = (int)(prob4*1000);
    double probs4 = (num4/1000.00);
    
    //calculates probability for five different cards
    double prob5 = (double)(count5)/(total);
    int num5 = (int)(prob5*1000);
    double probs5 = (num5/1000.00);
    
    //prints everything
 System.out.println("Total number of loops: " + total);
 System.out.println("The probability of Four-of-a-kind: "+ probs1);
 System.out.println("The probability of Three-of-a-kind: "+ probs2);
 System.out.println("The probability of Two Pairs: "+ probs3);
 System.out.println("The probability of One Pair: "+ probs4);
 System.out.println("The probability of no similar cards: "+ probs5);
    
    
    
    }
}
