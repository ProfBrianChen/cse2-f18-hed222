//I tried to submit this on time yesterday and I did but i ran out of free time so i couldn't turn the hw in on time please don't take of points!
import java.util.*;

public class CSE2Linear{ 
 
  public static void main(String[] args){ 
    Scanner scan = new Scanner (System.in); //creates scanner
    final int array[] = new int [15]; 
    System.out.println("Enter 15 ascending ints for final grades in CSE2:"); 
    for (int i = 0; i < 15;i++){ 
          boolean given = false; 

      while (given == false){ 
        if (scan.hasNextInt()){
          array[i] = scan.nextInt(); 
          given = true; 
        }
        else {
          System.out.println("Please make sure you enter an int.");
          scan.next();
        }
      }
      
      while (array[i] > 100 || array[i] < 0){ 
        System.out.println("Please make sure value is between 0 and 100.");
          array[i] = scan.nextInt();
      }
      if (i >0){
      while (array[i] < array[i - 1]){
       System.out.println("Please make sure the values are in ascending order.");
        array[i] = scan.nextInt();
        }
      }
      
      
       }
    //for loop prints array
    for (int i = 0; i<15; i++){
      int k = array[i];
      System.out.print(k + " ");
     }
    
    System.out.println();
    
    System.out.println("Enter a grade to search for:"); 
    binarySearch(array); 
    scramble(array); 
    System.out.print("Scrambled: "); 
    
    //prints scrambled array
     for (int i = 0; i<15; i++){ 
      int k = array[i];
      System.out.print(k + " ");
     }
    
    
    System.out.println("Enter a grade to search for:"); 

    linearSearch(array); 
    
  }
  
  //binary search
  public static void binarySearch(int[] arr){ 
    Scanner scan = new Scanner (System.in);
    int grade = scan.nextInt(); 
    int iterations = 0; 
    int low = 0; 
    int high = 14; 
    while (low <= high){
      int mid = (high + low)/2;
      if (grade == arr[mid]){
        iterations++;
        System.out.println(grade + " was found with " + iterations + " iterations.");
      }
      
      if (grade < arr[mid]){
        high = mid - 1;
        iterations++;
      }
      
      else {
        low = mid + 1;
        iterations++;
      }
    }
  }
  
  
  public static int[] scramble(int[] arr){
    Random r = new Random ();
    
    for (int i = 0; i < 15; i++){ 
      int p = r.nextInt(15);
     int t = arr[i];
      arr[i] = arr[p];
      arr[p]= t;
       }
    return arr;
  }
  
  //linear search
  public static void linearSearch(int[] arr){
    Scanner scan = new Scanner (System.in);
    int grade = scan.nextInt(); 
    int iterations = 0; 
    for (int i = 0; i < 15; i++){
      iterations++;
      if (arr[i] == grade){
        System.out.println(grade + " was found with " + iterations + " iterations.");
      }
    }
  }
  
}