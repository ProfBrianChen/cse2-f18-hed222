//I tried to submit this on time yesterday and I did but i ran out of free time so i couldn't turn the hw in on time please don't take of points!
import java.util.Scanner;
import java.util.*;
import java.util.Random;

public class RemoveElements{

  public static void main(String [] arg){
  Scanner scan=new Scanner(System.in);
  int num[]=new int[10];
  int newArray1[];
  int newArray2[];
  int index,target;
  String answer="";
  do{
    System.out.print("Random input 10 ints [0-9] ");
    System.out.println();
    num = randomInput();
    String out = "The original array is: ";
    out += listArray(num);
    System.out.println(out);
 
    System.out.print("Enter the index ");
    index = scan.nextInt();
    newArray1 = delete(num,index);
    String out1="The output array is ";
    out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
    System.out.println(out1);
 
      System.out.print("Enter the target value ");
    target = scan.nextInt();
    newArray2 = remove(num,target);
    String out2="The output array is ";
    out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
    System.out.println(out2);
     
    System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
    answer=scan.next();
  }
  while(answer.equals("Y") || answer.equals("y"));
}

public static int[] randomInput(){
  int[] array = new int[10];
  for(int i = 0; i < array.length; i++){
    int rand = (int) (Math.random() * 10);
    array[i] = rand;
  }
  return array;
}

public static int[] delete(int[] list, int pos){
  int[] array = new int[9];
    
    if(pos > list.length){
      System.out.println("The index is not valid");
    }
    for(int i = 0; i < pos; i++){
      array[i] = list[i];
    }
    for(int i = 0; i < 9; i++){
      if(i == pos){
        array[i] = list[i+1];
      }
    }
    for(int i = pos+1; i < 9; i++){
      array[i] = list[i+1];
    }
  return array;
}



public static int[] remove(int[] list, int target){
  int count = 0;
  for(int i = 0; i<list.length; i++){
    if(list[i]==target){
      count++;
    }
  }
    if(count==0){
      System.out.println("Item was not found");
      return list;
    }
    else{
      System.out.print("Element" + target+" was found.");
    }

    int[] array = new int[list.length-count];

    int c = 0;
    for(int i = 0; i<list.length-count; i++){
      if(array[i]!=target){
        array[i] = list[i];
        c++;
      }
    }
  
  return array;
}
 
  public static String listArray(int num[]){
  String out="{";
  for(int j=0;j<num.length;j++){
    if(j>0){
      out+=", ";
    }
    out+=num[j];
  }
  out+="} ";
  return out;
  }
}