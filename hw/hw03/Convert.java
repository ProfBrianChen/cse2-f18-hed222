// Haydn Davies
// CSE 002
//The purpose of this HW is to Convert the acres and number of inches of rain affected by a huricane into cubic miles 
//This is for any input a user gives

import java.util.Scanner; //imports scanner from java

public class Convert{
  
  public static void main(String[] args){
    
    Scanner myScanner = new Scanner(System.in); // initiates the scanner class it allows users to input data when prompted
    System.out.print("Enter the affected area in acres(to the .xx decimal place): ");
    double acres = myScanner.nextDouble(); //Assigns the double that the user inputed to this variable
    System.out.print("Enter the rainfall in the affected area(in # of inches): ");
    double rainfall = myScanner.nextDouble();
    
    
    
    //double a = Math.pow(640.0, 1.0/2.0); //acre in terms of miles
    double squareMiles = acres*0.0015625;//converts acres to square miles
    double miles = rainfall*0.0000157828;//converts inches of rainfall to miles
    double total = miles*squareMiles; //creates cubic miles by multiply square miles by miles of rainfall
    
//     double totalGallons = (rainfall*27154.3*acres);//returns totalGallons of rain fall. (total Inches of rain)*(amount of gallons in one acre)*(total acres)
//    // double a = cubicMile*rainfall;
//     //double a = Math.pow(totalGallons, 1.0 / 3.0); //takes the cube root of the totalGallons
//     //double cubicAcre = rainfall*acres;
//     double cubicMile = totalGallons*(.00000000000908169); 
//     double total = Math.pow(cubicMile, 1.0 / 3.0);
    
    System.out.println("Quantity of rain in cubic miles: "+ total);
    
    
  }
}
