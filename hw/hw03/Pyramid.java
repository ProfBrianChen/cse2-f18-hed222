// Haydn Davies
// CSE 002
//The purpose for this hw is to ask the users for dimensions of a pyramid, and it will calcutlate its volume

import java.util.Scanner; //imports scanner from java

public class Pyramid{
  
  public static void main(String[] args){
    
    Scanner myScanner = new Scanner(System.in); // initiates the scanner class it allows users to input data when prompted
    System.out.print("The square side of the pyramid is (input side1): ");
    double side1 = myScanner.nextDouble(); //Assigns the double that the user inputed to this variable
    System.out.print("The square side of the pyramid is (input side2): ");
    double side2 = myScanner.nextDouble(); //Assigns the double that the user inputed to this variable
    System.out.print("The height of the pyramid is (input height): ");
    double height = myScanner.nextDouble();//Assigns the double that the user inputed to this variable
    double volume = (side1*side2*height)/(3.0); // gets the volume for the pyramid    
    System.out.println("The volume inside the pyramid is: "+ volume);
  }
}
