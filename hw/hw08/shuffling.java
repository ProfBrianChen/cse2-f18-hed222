import java.util.Scanner;
import java.util.Random;

public class shuffling{ 

	public static void main(String[] args) { 
		Scanner scan = new Scanner(System.in); 
		//suits club, heart, spade or diamond 
		String[] suitNames={"C","H","S","D"};    
		String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
		String[] cards = new String[52]; 
		String[] hand = new String[5]; 
		int numCards = 5; //needs to be 5 not 0, because this is the number of cards in our hand
		int again = 1; 
		int index = 51;

		for (int i=0; i<52; i++){ 
			cards[i]=rankNames[i%13]+suitNames[i/13]; 	
		} 

		printArray(cards); 
		shuffle(cards); 
		printArray(cards); 

		while(again == 1){ 
		   hand = getHand(cards,index,numCards); 
		   printArray(hand);
		   index = (index - numCards);
		   
      //checks to see if there aren't enough cards to make a hand, if not calls shuffling and prints it
      if(index<numCards){
		   	index=51;
		   	shuffle(cards);
		   	printArray(cards);
		   }
		   System.out.println("Enter a 1 if you want another hand drawn"); 
		   again = scan.nextInt(); 
		}  

	} 
  //prints any array that it is fed . no matter the length 
	public static void printArray(String[] list){
    for(int i = 0; i<list.length; i++){
			System.out.print(list[i]+" ");
		}
		System.out.println();
	}
 // shuffles any array no matter the length
	public static void shuffle(String[] list){
		System.out.println();
		Random rand = new Random();

		for(int i = 0; i<list.length; i++){
			int r = i + rand.nextInt(52 - i); //random int
			
      //switches elements randomly with each other
      String temp = list[r]; 
      list[r] = list[i]; 
      list[i] = temp;
		}
		System.out.println("Shuffled: ");
	}
	public static String[] getHand(String[] list, int index, int numCards){
		//returns the array of five cards that is your hand
    String[] arr = new String[numCards];
		System.out.println();
		System.out.println("Hand: ");
		//int count = 0;
		for(int i = 0; i<numCards; i++){
			arr[i] = list[index-i]; //sets the arr at i to index minus the vakue of i.

		}
		return arr;
	}
}
