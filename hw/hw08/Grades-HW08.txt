
Grading Sheet for HW8

Grade:100

Compiles    				20/20 pts
Comments 				10/10 pts
Method shuffle(list)			20/20 pts
Method getHand(list,index, numCards)  20/20 pts

Method printArray(list).                        20/20 pts

Checks If numCards given is greater than the number of cards in the deck and if so creates a new deck of cards.             10/10 pts

