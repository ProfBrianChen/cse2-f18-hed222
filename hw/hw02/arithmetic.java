//Haydn Davies
//CSE 002 HW02

public class arithmetic{
  
  public static void main(String args[]){
    int numPants = 3; //Number of pairs of pants
    double pantsPrice = 34.98; //Cost per pair of pants
    int numShirts = 2; //Number of sweatshirts
    double shirtPrice = 24.99; //Cost per shirt
    int numBelts = 1; //Number of belts
    double beltCost = 33.99; //cost per belt
    double paSalesTax = 0.06; //the tax rate
    
    
    double totalCostOfPants = numPants*pantsPrice;   //total cost of pants
    double totalCostOfSweatshirts = numShirts*shirtPrice;   //total cost of sweatshirts
    double totalCostOfBelts = numBelts*beltCost; //total cost of belts
    
    double BeltsalesTax = paSalesTax*totalCostOfBelts; // Sales tax on Belts
    double shirtSalesTax = paSalesTax*totalCostOfSweatshirts; // Sales tax on sweatshirts
    double PantSalesTax = paSalesTax*totalCostOfPants; // Sales tax on Pants
    
    double totalCost = totalCostOfBelts+totalCostOfSweatshirts+totalCostOfPants; //total cost of all items before sales tax
    double totalTax = paSalesTax*totalCost; //Sales tax on all items
    double totalPrice = totalCost+totalTax; //What the final Price is
  
    int myprice = (int)(totalPrice*100); //moves decimal point two places
    double price = (myprice/100.0); //makes it an accurate number again
    
    System.out.println("Pants total Cost: $" + totalCostOfPants); // prints total cost of pants
    System.out.println("Sweatshirts total Cost: $" + totalCostOfSweatshirts); // prints total costg of sweatshirts
    System.out.println("Belts total Cost: $" + totalCostOfBelts); // prints total cost of belts
    
    System.out.println("Belt Sales Tax: $" + BeltsalesTax); // prints tax on belts
    System.out.println("Pants Sales Tax: $" + PantSalesTax); // prints tax on pants
    System.out.println("sweatshirts Sales Tax: $" + shirtSalesTax); // prints tax on sweatshirts
    
    
    System.out.println("Total Cost: $" + totalCost); //prints Total Cost before tax
    System.out.println("Final Tax: $" + totalTax); // prints Total tax on the sale
    System.out.println("Price before rounding: $" +totalPrice); // price before rounding
    System.out.println("Final Price: $" + price); // prints final amount due. Price
    
    
  }
  
}