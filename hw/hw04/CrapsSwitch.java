//Haydn Davies
//CSE 002


import java.util.*;
import java.util.Scanner;

public class CrapsSwitch{
  
  public static void main(String args[]){
    Scanner myScanner = new Scanner(System.in); // initalizes scanner
    int rand1 = (int)(Math.random()*6+1); // creates a random variable from 1-6
    int rand2 = (int)(Math.random()*6+1); // creates a random variable from 1-6
    int randomPair = rand1*rand2;
    System.out.print("Do you want to check two random dice? (yes type 1 or no type 2) "); //prints a text with directions
    int response = myScanner.nextInt();

    if(response==2){
      System.out.print("What is the value of die 1? "); //prints a text with directions
      int die1 = myScanner.nextInt();
      System.out.print("What is the value of die 2? "); //prints a text with directions
      int die2 = myScanner.nextInt();
      int Pair=die1+die2;
    
      switch(Pair){
        case 2:
          System.out.println("You rolled snake eyes (two 1's)");
          break;
        case 3:
          System.out.println("You rolled an Ace Deuce (1 and 2)");
          break;
        case 4:
          if(die1==1||die1==3){
            System.out.println("You rolled an easy four (1 and 3)");
          }
          else{
            System.out.println("You rolled a hard four (two 2's)");
          }
          break;
        case 5:
          if(die1==2||die1==3){
            System.out.println("You rolled a Fever Five (2 and 3)");          }
          else{
            System.out.println("You rolled a Fever Five (1 and 4)");          }
          break;
        case 6:
          if(die1==2||die1==4){
            System.out.println("You rolled an easy six (4 and 2)");
          }
          else{
            System.out.println("You rolled a hard six (two 3's)");
          }
          break;
        case 7:
          if(die1==3||die1==4){
            System.out.println("You rolled a seven out (4 and 3)");
          }
          else{
            System.out.println("You rolled a seven out (5 and 2)");
          }          
          break;
        case 8:
          if(die1==3||die1==5){
            System.out.println("You rolled an easy eight (5 and 3)");
          }
          else{
            System.out.println("You rolled a hard eight (two 4's)");
          }              
          break;
        case 9:
          System.out.println("You rolled a nine (4 and 5)");
          break;
        case 10:
           if(die1==4||die1==6){
            System.out.println("You rolled an easy ten (4 and 6)");
          }
          else{
            System.out.println("You rolled a hard ten (two 5's)");
          }      
          break;
        case 11:
          System.out.println("You rolled a yo-leven (5 and 6)");
          break;
        case 12:
          System.out.println("You rolled Boxcars (two 6's)");
          break;
      }
    }
    else{
      switch(randomPair){
        case 1:
          System.out.println("You rolled snake eyes (two 1's)");
          break;
        case 2:
          System.out.println("You rolled an Ace Deuce (1 and 2)");
          break;
        case 3:
          System.out.println("You rolled an easy four (1 and 3)");
          break;
        case 4:
          System.out.println("You rolled a hard four (two 2's)");
          break;
        case 5:
          System.out.println("You rolled a Fever Five (2 and 3)");
          break;
        case 6:
          System.out.println("You rolled a hard six (two 3's)");
          break;
        case 7:
          System.out.println("You rolled a Fever Five (1 and 4)");
          break;
        case 8:
          System.out.println("You rolled an easy six (4 and 2)");
          break;
        case 9:
          System.out.println("You rolled a seven out (4 and 3)");
          break;
        case 10:
          System.out.println("You rolled a hard eight (two 4's)");
          break;
        case 11:
          System.out.println("You rolled an easy six (1 and 5)");
          break;
        case 12:
          System.out.println("You rolled a seven out (5 and 2)");
          break;
        case 13:
          System.out.println("You rolled an easy eight (5 and 3)");
          break;
        case 14:
          System.out.println("You rolled a nine (4 and 5)");
          break;
        case 15:
          System.out.println("You rolled a hard ten (two 5's)");
          break;
        case 16:
          System.out.println("You rolled a seven out (1 and 6)");
          break;
        case 17:
          System.out.println("You rolled an easy eight (2 and 6)");
          break;
        case 18:
          System.out.println("You rolled a nine (3 and 6)");
          break;
        case 19:
          System.out.println("You rolled an easy ten (4 and 6)");
          break;
        case 20:
          System.out.println("You rolled a yo-leven (5 and 6)");
          break;
        case 21:
          System.out.println("You rolled Boxcars (two 6's)");
          break;
       case 22:
          System.out.println("You rolled an Ace Deuce (1 and 2)");
          break;
        case 23:
          System.out.println("You rolled an easy four (1 and 3)");
          break;
        case 24:
          System.out.println("You rolled a fever five (2 and 3)");
          break;
        case 25:
          System.out.println("You rolled an easy five (1 and 4)");
          break;
        case 26:
          System.out.println("You rolled a seven out (1 and 6)");
          break;
        case 27:
          System.out.println("You rolled an easy six (1 and 5)");
          break;
        case 28:
          System.out.println("You rolled an easy six (2 and 4)");
          break;
        case 29:
          System.out.println("You rolled a seven out (2 and 5)");
          break;
        case 30:
          System.out.println("You rolled an easy eight (2 and 6)");
          break;
        case 31:
          System.out.println("You rolled a seven out (3 and 4)");
          break;
        case 32:
          System.out.println("You rolled a nine (3 and 6)");
          break;
        case 33:
          System.out.println("You rolled an easy eight (5 and 3)");
          break;
        case 34:
          System.out.println("You rolled a nine (4 and 5)");
          break;
        case 35:
          System.out.println("You rolled an easy ten (4 and 6)");
          break;
        case 36:
          System.out.println("You rolled a yo-leven (5 and 6)");
          break;
      }
    }
  }
}