//Haydn Davies
// HW07
// Word Tools

//imports scanner
import java.util.Scanner;

//creates a class
public class wordTools{
  
  
  //prompts the user to create a sample text
  public static String sampleText(){
     Scanner scan = new Scanner(System.in);
     System.out.println("Plase enter a string.");
     String str = scan.nextLine();
     return str;
  } 
  //runs the print menu, and returns a string of what the user wants to do with the menu
  public static String printMenu(){
      Scanner scan = new Scanner(System.in);
      System.out.println(" ");
      System.out.println("Menu");
      System.out.println(" ");
      System.out.println("c - Number of non-whitespace characters");
      System.out.println("w - Number of words");
      System.out.println("f - Find text");
      System.out.println("r - Replace all !'s");
      System.out.println("s - Shorten spaces");
      System.out.println("q - Quit");
      System.out.println("Choose an option: (Please hit return twice) ");
      String s = scan.nextLine();
      
      boolean correct = false;
      while(correct==false){
        if(s.equals("c")||s.equals("w")||s.equals("f")||s.equals("r")||s.equals("s")||s.equals("q")){
          correct = true;
        }
        else{
          System.out.println("Please enter a correct string");
          correct = false; 
          break;
        }
      
          if(s.equals("c")){
              return "c";
          }
          else if(s.equals("w")){
              return "w";
          }
          else if(s.equals("f")){
              return "f";
          }
          else if(s.equals("r")){
              return "r";
          }
          else if(s.equals("s")){
              return "s";
          }
          else if(s.equals("q")){
              quit();
          }
      }
       return s;
 
  }

  //quits a program
  public static void quit(){
    System.exit(0);
  }
  //counts the number of characterws in a string and retunrs it
  public static int getNumOfNonWSCharacters(String input){
    Scanner scan = new Scanner(System.in);
    String s = scan.nextLine();
    int space = 0;
    for(int i = 0; i < input.length(); i++){
      if(input.charAt(i)== ' '){
        space++;
      }
    }
    int ans = input.length()-space;
    return ans;
  }
//counts the number of words in the string and returns it
  public static int getNumOfWords(String input){
    Scanner scan = new Scanner(System.in);
    String s = scan.nextLine();
    int count = 0;
    for (int i = 0; i < input.length(); i++){
      if (input.charAt(i) != ' '){
        count++;
        while(input.charAt(i) != ' ' && i < input.length() - 1){
          i++;
        }
      }
    }
    return count;
  }

//finds an inputed word that is desired by the user
  public static int findText(String input, String wantedText){
    int count = input.replaceAll("[^"+ wantedText+ "]", "").length()/wantedText.length();
    if(count>0){
      
      System.out.println(wantedText+ " occurs "+count+" times in the string.");
    }
    else{
      System.out.println(wantedText+ " did not occur in string.");
    }
    return count;
  }

//replaces an exclamation point with a period
  public static String replaceExclamation(String input){
    Scanner scan = new Scanner(System.in);
    String s = scan.nextLine();
    return input.replaceAll("!", ".");
  }
//reduces multiple spaces to one
  public static String shortenSpace(String input){
    Scanner scan = new Scanner(System.in);
    String after = input.trim().replaceAll(" +", " ");
    return after;
  }

  //is. my main class where everything is run and printed
  public static void main(String args[]){
      Scanner scan = new Scanner(System.in);
      String str = sampleText();
      System.out.println("You entered: "+  str);
      String choice = " ";
      choice = printMenu();
      if (choice.equals("c")){
        int spaces = getNumOfNonWSCharacters(str);
        System.out.println("Number of non-whitespace characters: " + spaces);
      }
      else if (choice.equals("w")){
        int numWords = getNumOfWords(str);
        System.out.println("Number of words: " + numWords);
      }
      else if (choice.equals("f")){
        System.out.println("Enter a word to be found: ");
        String findText = scan.next();
        int run = findText(str, findText);
      }
      else if (choice.equals("r")){
        String replace = replaceExclamation(str);
        System.out.println("Replace all !'s: " + replace);
      }
      else if (choice.equals("s")){
        String shorten = shortenSpace(str);
        System.out.println("New output: "+shorten);
      }      
  }
}
