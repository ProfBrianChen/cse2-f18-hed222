//Haydn Davies
//CSE 002
//This program will pick a randomly generated number between 1-52 (inclusive). Then tell the user what card they picked so they can practice their magic

import java.util.*;

public class CardGenerator{
  
  public static void main(String args[]){
    int rand = (int)((Math.random()*52)+2); //assigns a random number from 1-52

    if(rand <14){
      switch(rand){
        case 1:
          System.out.println("You picked the Ace of Diamonds");
          break;
        case 2:
          System.out.println("You picked the two of Diamonds");
          break;
        case 3:
          System.out.println("You picked the three of Diamonds");
          break;
        case 4:
          System.out.println("You picked the four of Diamonds");
          break;
        case 5:
          System.out.println("You picked the five of Diamonds");
          break;
        case 6:
          System.out.println("You picked the six of Diamonds");
          break;
        case 7:
          System.out.println("You picked the seven of Diamonds");
          break;
        case 8:
          System.out.println("You picked the eight of Diamonds");
          break;
        case 9:
          System.out.println("You picked the nine of Diamonds");
          break;
        case 10:
          System.out.println("You picked the ten of Diamonds");
          break;
        case 11:
          System.out.println("You picked the Jack of Diamonds");
          break;
        case 12:
          System.out.println("You picked the Queen of Diamonds");
          break;
        case 13:
          System.out.println("You picked the King of Diamonds");
          break;
      }
    }
    else if(rand<27){
      switch(rand){
        case 14:
          System.out.println("You picked the Ace of Clubs");
          break;
        case 15:
          System.out.println("You picked the two of Clubs");
          break;
        case 16:
          System.out.println("You picked the three of Clubs");
          break;
        case 17:
          System.out.println("You picked the four of Clubs");
          break;
        case 18:
          System.out.println("You picked the five of Clubs");
          break;
        case 19:
          System.out.println("You picked the six of Clubs");
          break;
        case 20:
          System.out.println("You picked the seven of Clubs");
          break;
        case 21:
          System.out.println("You picked the eight of Clubs");
          break;
        case 22:
          System.out.println("You picked the nine of Clubs");
          break;
        case 23:
          System.out.println("You picked the ten of Clubs");
          break;
        case 24:
          System.out.println("You picked the Jack of Clubs");
          break;
        case 25:
          System.out.println("You picked the Queen of Clubs");
          break;
        case 26:
          System.out.println("You picked the King of Clubs");
          break;
      }
    }
    else if(rand<40){
      switch(rand){
        case 27:
          System.out.println("You picked the Ace of Hearts");
          break;
        case 28:
          System.out.println("You picked the two of Hearts");
          break;
        case 29:
          System.out.println("You picked the three of Hearts");
          break;
        case 30:
          System.out.println("You picked the four of Hearts");
          break;
        case 31:
          System.out.println("You picked the five of Hearts");
          break;
        case 32:
          System.out.println("You picked the six of Hearts");
          break;
        case 33:
          System.out.println("You picked the seven of Hearts");
          break;
        case 34:
          System.out.println("You picked the eight of Hearts");
          break;
        case 35:
          System.out.println("You picked the nine of Hearts");
          break;
        case 36:
          System.out.println("You picked the ten of Hearts");
          break;
        case 37:
          System.out.println("You picked the Jack of Hearts");
          break;
        case 38:
          System.out.println("You picked the Queen of Hearts");
          break;
        case 39:
          System.out.println("You picked the King of Hearts");
          break;
      }
    }
    else if(rand<52){
      switch(rand){
        case 40:
          System.out.println("You picked the Ace of Spades");
          break;
        case 41:
          System.out.println("You picked the two of Spades");
          break;
        case 42:
          System.out.println("You picked the three of Spades");
          break;
        case 43:
          System.out.println("You picked the four of Spades");
          break;
        case 44:
          System.out.println("You picked the five of Spades");
          break;
        case 45:
          System.out.println("You picked the six of Spades");
          break;
        case 46:
          System.out.println("You picked the seven of Spades");
          break;
        case 47:
          System.out.println("You picked the eight of Spades");
          break;
        case 48:
          System.out.println("You picked the nine of Spades");
          break;
        case 49:
          System.out.println("You picked the ten of Spades");
          break;
        case 50:
          System.out.println("You picked the Jack of Spades");
          break;
        case 51:
          System.out.println("You picked the Queen of Spades");
          break;
        case 52:
          System.out.println("You picked the King of Spades");
          break;
      }
    }
  }
}