//Haydn Davies 
//Lab09
//CSE002

import java.util.*;

public class passingArrays{

	public static void main(String[] args){
		int[] a = {1,2,3,4,5,6,7,8};
		int[] a1 = copy(a);
		int[] a2 = copy(a);
		invert(a);
		print(a);
		invert2(a1);
		print(a1);
		int[] a3 = invert2(a2);
		print(a3);

	}

  //prints arrays
	public static void print(int[] arr){
		for(int i = 0; i< arr.length; i++){
			int j = arr[i];
			System.out.print(j + " ");
		}
		System.out.println();
	}

  //copy's arrays
	public static int[] copy(int[] arr){
		int[] a1 = new int[arr.length];
		for(int i = 0; i < arr.length; i++){
			a1[i] = arr[i];
		}
		return a1;
	}

  //inverts arrays
	public static void invert(int[] arr){
		int[] a1 = copy(arr);
		for(int i = 0; i < arr.length; i++){
			arr[i] = a1[arr.length - i -1];
		}
	}

  //inverts arrays from the first half to mirror the second half
	public static int[] invert2(int[] arr){
		int[] a2 = copy(arr);
		for(int i = 0; i < a2.length/2; i++){
			a2[i] = a2[a2.length-i-1];
		}
		return a2;
	}
 
}
