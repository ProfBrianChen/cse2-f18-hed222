//Haydn Davies
//lab07
// Methods

import java.util.Random;
import java.util.Scanner;

public class methods{
  public static String adjectives(){
    Random rand =  new Random(); // creates a random number
    int randomInt = rand.nextInt(10); // gives us a random integer that is less than 10
    String adj ="";
    switch(randomInt){
      case 1: adj = "brave";
        break;
      case 2: adj = "happy";
        break;
      case 3: adj = "calm";
        break;
      case 4: adj = "small";
        break;
      case 5: adj = "big";
        break;
      case 6: adj = "old";
        break;
      case 7: adj = "young";
        break; 
      case 8: adj = "sad";
        break; 
      default: adj = "handsome";
   }
    return adj;
  }
  public static String subjects(){
   Random rand =  new Random(); // creates a random number
    int randomInt = rand.nextInt(10); // gives us a random integer that is less than 10
    String sub = "";
    switch(randomInt){
      case 1: sub = "dog";
        break;
      case 2: sub = "cat";
        break;
      case 3: sub = "man";
        break;
      case 4: sub = "person";
        break;
      case 5: sub = "woman";
        break;
      case 6: sub = "father";
        break;
      case 7: sub = "mother";
        break; 
      case 8: sub = "nurse";
        break; 
      default: sub = "doctor";
    }    
    return sub;
  }
  public static String verbs(){
    Random rand =  new Random(); // creates a random number
    int randomInt = rand.nextInt(10); // gives us a random integer that is less than 10
    String vrb = "";
    switch(randomInt){
        case 1: vrb= "gave";
        break;
      case 2: vrb = "ran";
        break;
      case 3: vrb = "hid";
        break;
      case 4: vrb = "ate";
        break;
      case 5: vrb = "laughed";
        break;
      case 6: vrb = "talked";
        break;
      case 7: vrb = "threw";
        break; 
      case 8: vrb = "made";
        break; 
      default: vrb = "fell";
    }
    return vrb;
  }
  public static String objects(){
    Random rand =  new Random(); // creates a random number
    int randomInt = rand.nextInt(10); // gives us a random integer that is less than 10
    String obj = "";
    switch(randomInt){
        case 1: obj = "cannon";
        break;
      case 2: obj = "cactus";
        break;
      case 3: obj = "island";
        break;
      case 4: obj = "seashore";
        break;
      case 5: obj = "throne";
        break;
      case 6: obj = "vegetable";
        break;
      case 7: obj = "toothbrush";
        break; 
      case 8: obj = "lamp";
        break; 
      default: obj = "finger";
        
    }
    return obj;
  }
  public static String paragraph(){
      String sub = subjects();
      String sentence = ("The " + adjectives() + " " + sub + " " + verbs() + " the " + adjectives() + " " + objects() + ".");
      String sentence1  = ("It "+verbs()+ " the " + sub + " and the "+ sub + " " + verbs()+". ");
      String sentence2 = ("That " + sub + " "+ verbs() + " the " + objects()+"!");
      return sentence+" "+sentence1+" "+sentence2;
  }
  
  
  public static void main(String args[]){
    String sub = subjects();
    Scanner scan = new Scanner(System.in); // initializes the scanner
    // System.out.println("Would you like to make a sentence? (Answer 'y' for yes and 'n' for no");
    // System.out.println("The " + adjectives() + " " + sub + " " + verbs() + " the " + adjectives() + " " + objects() + ".");
    System.out.println(paragraph());
  }
}
