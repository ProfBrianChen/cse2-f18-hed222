//Haydn Davies
// Date 9.6.2018
// CSE 002-311
// Lab 002
//My program will record time elapsed in seconds and the number of front wheel rotations in that time
//The program is meant to print the number of miles in each trip and the time of each trip

public class Cyclometer{
  
  public static void main(String args[]){
    int secsTrip1=480;  // sets an int value of trip one time to 480 seconds
    int secsTrip2=3220;  // sets an int value of trip 2 time to 3220 seconds
    int countsTrip1=1561;  // sets an int value of the number of front wheel roatations to 1561 in trip 1
    int countsTrip2=9037; // sets an int value of the number of front wheel roatations to 9037 in trip 2
    double wheelDiameter=27.0,  // tells us the diameter of the wheel; sets it to a double 27.0
    PI=3.14159, // makes PIE an easy variable to use for the long number, it is also a double
  	feetPerMile=5280,  // it is a double, gives us the feet value for each mile
  	inchesPerFoot=12,   // double that gives us the inch value for each foot
  	secondsPerMinute=60;  // double that defines the number of seconds in a minute
	  double distanceTrip1, distanceTrip2,totalDistance;  // defines the trip distances and total destances as a double

    System.out.println("Trip 1 took " + (secsTrip1/secondsPerMinute) +" minutes and had " + countsTrip1+" counts."); // print out the time for trip 1 in minutes and the number of counts it took
	  System.out.println("Trip 2 took " + (secsTrip2/secondsPerMinute) +" minutes and had " + countsTrip2+" counts."); // print out the time for trip 2 in minutes and the number of counts it took
    distanceTrip1=countsTrip1*wheelDiameter*PI;
    	// Above gives distance in inches
    	//(for each count, a rotation of the wheel travels
    	//the diameter in inches times PI)
    distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles
    distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile; // computes distance for trip 2
    totalDistance=distanceTrip1+distanceTrip2; // computes totalDistance of both trips
    //Print out the output data.
    System.out.println("Trip 1 was "+distanceTrip1+" miles"); // prints the distanceTrip1 in miles
    System.out.println("Trip 2 was "+distanceTrip2+" miles"); // prints the distanceTrip2 in miles
    System.out.println("The total distance was "+totalDistance+" miles"); // prints the totalDistance in miles


    
  }
  
}